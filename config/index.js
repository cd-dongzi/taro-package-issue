const path = require('path')
const componentChunk = 'pages/packageA/demo'
const config = {
  projectName: 'taro-issue',
  date: '2020-12-17',
  designWidth: 750,
  deviceRatio: {
    '640': 2.34 / 2,
    '750': 1,
    '828': 1.81 / 2
  },
  sourceRoot: 'src',
  outputRoot: 'dist',
  alias: {
    '@': path.resolve(__dirname, '..', 'src')
  },
  babel: {
    sourceMap: true,
    presets: [
      ['env', {
        modules: false
      }]
    ],
    plugins: [
      'transform-decorators-legacy',
      'transform-class-properties',
      'transform-object-rest-spread',
      ['transform-runtime', {
        'helpers': false,
        'polyfill': false,
        'regenerator': true,
        'moduleName': 'babel-runtime'
      }]
    ]
  },
  copy: {
    patterns: [
      {from: 'sitemap.json', to: `dist/sitemap.json`},
    ]
  },
  plugins: [],
  defineConstants: {
  },
  mini: {
    webpackChain: (chain, webpack) => {
      const config = chain.optimization.get('splitChunks')
      chain.merge({
        optimization: {
          splitChunks: {
            ...config,
            cacheGroups: {
              ...config.cacheGroups,
              [componentChunk]: {
                name: componentChunk,
                minChunks: 1,
                test: module => {
                  return /[\\/]components[\\/]Demo[\\/]/.test(module.resource)
                },
                priority: 200
              }
            }
          }
        }
      })
      chain.plugin('analyzer')
        .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])
    },
    addChunkPages(pages, pagesNames) {
      pages.set('pages/packageA/a/index', [componentChunk])
      console.log('addChunkPages', pages, pagesNames)
    },
    postcss: {
      pxtransform: {
        enable: true,
        config: {}
      },
      url: {
        enable: true,
        config: {
          limit: 10240 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  },
  h5: {
    publicPath: '/',
    staticDirectory: 'static',
    postcss: {
      autoprefixer: {
        enable: true,
        config: {
          browsers: [
            'last 3 versions',
            'Android >= 4.1',
            'ios >= 8'
          ]
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  }
}

module.exports = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
