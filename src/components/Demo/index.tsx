import Taro, { ComponentOptions } from '@tarojs/taro'
import { View } from '@tarojs/components'

type Props = {}

interface Demo {
  (props: Props): JSX.Element | null
  options?: ComponentOptions
  defaultProps?: any
}

const Demo: Demo = () => {
  return (
    <View className="Demo">
      Demo
    </View>
  )
}

Demo.defaultProps = {}
Demo.options = {}
export default Demo