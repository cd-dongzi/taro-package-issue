import Taro, { ComponentOptions } from '@tarojs/taro'
import { View } from '@tarojs/components'

type Props = {}

interface A {
  (props: Props): JSX.Element | null
  options?: ComponentOptions
  defaultProps?: any
}

const A: A = () => {
  return (
    <View className="A">
      PackageB === A
    </View>
  )
}

A.defaultProps = {}
A.options = {}
export default A